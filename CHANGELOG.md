# Changelog
All notable changes to this project will be documented in this file.

## [v6.0.0] - 2024-09-12

### Chore
- **💥 BREAKING**: Re-scaffolded the addon v2 structure using [addon-blueprint](https://github.com/embroider-build/addon-blueprint), which cleaned up the project a bit, but also bumped a lot of dependencies.
- **💥 BREAKING**: Bumped version of **ember-intl** to [v7](https://ember-intl.github.io/ember-intl/versions/v7.0.6/docs/migration/v7) and using code that is no longer compatible with `ember-intl@<7.x`.
- **💥 BREAKING**: Moved addon code to TypeScript.

## [v5.1.1] - 2024-09-12

### Added

- Test coverage to make sure future development is stable.
- Documentation about known limits of changing locale.

### Chore

- Bumped package versions in the test app.
- Switched from `yarn` to `pnpm`.
- Simplified translation lookup logic.

## [v5.1.0] - 2024-06-14

### Fixed

- Relaxed `peerDependecies` on `ember-intl` to include `v5.x` and `v6.x`. 

## [v5.0.0] - 2022-08-25

- **💥 BREAKING**: Update to ember addon v2 format using amazing [ember-addon-mirator](https://github.com/NullVoxPopuli/ember-addon-migrator) and help of the folks on Ember Discord.
- **💥 BREAKING**: Tons of package version udpates, including Node version upgrades.

## [v4.0.0] - 2022-02-11

### Chore
- Updates in linters.

### Fixed
- Dummy app was broken because of missing `ember-data` dependency.
- Dummy app input types were used incorrectly.
- Dummy app was missing CS translation files, making it quite useless.

### Changed
- **BREAKING**: Dopeed node-10 support.
- `ember-cli` updated `3.26.1 -> 4.1.0`.
- `ember-intl` updated `5.6.2 -> 5.7.2`.
- `ember-changeset-validations` updated `3.14.7 -> 3.16.0`
- `ember-cli-babel` updated `7.26.11 -> 7.26.6`
- `ember-cli-htmlbars` udpated `5.7.1 -> 6.0.1`

## [v3.0.2] - 2021-06-27
### Chore
- Removed IE11 compatibility for the dummy app.
- Moved repo to new Group in GitLab

## [v3.0.1] - 2021-06-20

### Fixed
- Removed dummy `cs` translation that caused the pacakge to yield `[ember-intl] "validations.greaterThan" was not found in "cs"` errors on build.
## [v3.0.0] - 2020-02-06
### Changed
- **BREAKING**: Bumped version of ember-cli to v3.24, which *might* break some things for some projects.
- A lot of improvements in the README.md to make the DX more pleasant.
- Moved `CHANGELOG` to `CHANGELOG.md`.

### Added
- Complete example in dummy app.

## [v2.0.1] - 2020-09-11
### Fixed
- Fix `lookup` function signature compatibility, ember-intl >= 5.4.0

## [v2.0.0] - 2020-07-19
### Changed
- **BREAKING**: Bumped version of ember-intl
- **BREAKING**: Bumped version of ember-changeset-validations
- **BREAKING**: Changed the code that extracts messages from JSON file, which _needs_ to be accompanied with `ember-changeset-validations >= v3.x` and `ember-intl >= v5.x`.

## [v1.0.0] - 2019-07-28
### Changed
- Bump `ember-intl` and `ember-changeset-validations` versions
- Bump this package version to signify that things might change with this big jump

## [v0.1.4] - 2018-11-25
### Removed
- Remove ember-data as a dependency

### Added
- Add no-bare-strings for .template-lintrc.js

### Fixed
- Tests run in Chromium
- Make sure that all strings are translated

## [v0.1.3] - 2018-09-27
### Fixed
 - Remove example translation file - This would be pulled into your project and might override your own values

## [v0.1.2] - 2018-09-26
### Added
- Translation can have placeholders and they are properly interpreted
- Add dummy test app

## [v0.1.1] - 2018-09-26
### Fixed
 - Remove forgotten console.log

## [v0.1.0] - 2018-09-26
### Added
- First release
