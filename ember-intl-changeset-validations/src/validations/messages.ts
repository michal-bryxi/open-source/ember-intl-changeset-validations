// @ts-expect-error No TS yet
import defaultMessages from 'ember-changeset-validations/utils/messages';
import IntlService from 'ember-intl/services/intl';

import application from '../utils/application.ts';

const intl = application.instance?.lookup('service:intl') as IntlService;

const messages: { [key: string]: string } = {};

Object.keys(defaultMessages).forEach((key) => {
  const customMessage = intl.getTranslation(
    `validations.${key}`,
    intl.primaryLocale ?? '',
  );
  const defaultMessage = defaultMessages[key];

  messages[key] = customMessage ?? defaultMessage;
});

export default messages;
