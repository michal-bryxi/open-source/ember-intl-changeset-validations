export VOLTA_FEATURE_PNPM=1
export VOLTA_HOME="$CI_PROJECT_DIR/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
alias google-chrome='chromium'
